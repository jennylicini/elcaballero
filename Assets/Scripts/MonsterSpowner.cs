using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MonsterSpowner : MonoBehaviour
{

    [SerializeField]
    private GameObject[] monsterReference;

    private GameObject spawnedMonster;

    [SerializeField]
    private Transform leftPos, rightPos;

    private int randomIndex;
    private int randomSide;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnMonsters());
        //setScore();
    }

    // Update is called once per frame
    IEnumerator SpawnMonsters() {

        while (true) {

            yield return new WaitForSeconds(Random.Range(2, 3)); //es el tiempo de cada monstro

            randomIndex = Random.Range(0, monsterReference.Length);
            randomSide = Random.Range(0, 2);

            spawnedMonster = Instantiate(monsterReference[randomIndex]);


            // left side
            if (randomSide == 0)
            {

                spawnedMonster.transform.position = leftPos.position;
                spawnedMonster.GetComponent<Monster>().speed = Random.Range(4, 6);

                 if (randomIndex == 1){
                    spawnedMonster.transform.localScale = new Vector3(-3f, 3f, 0f);
                }

            }
            else
            {
                //spawnedMonster = Instantiate(monsterReference[randomIndex]); // de momento lo pongo solo aqui 
                // right side
                spawnedMonster.transform.position = rightPos.position;
                spawnedMonster.GetComponent<Monster>().speed = -Random.Range(4, 6);

                if (randomIndex == 0){
                    spawnedMonster.transform.localScale = new Vector3(-0.8f, 0.8f, 0f);

                }

            }

        } // while loop

    }

   
}
