using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float jumpForce = 10f;
    [SerializeField]
    private float moveForce = 11f;
    public float movementX;
    private Rigidbody2D myBody;
    public SpriteRenderer sr;
    private Animator anim;
    private string WALK_ANIMATION = "Walk";
    private bool isGrounded;
    private string GROUND_TAG = "Ground";
    private string ENEMY_TAG = "Enemy";
    private string GHOST_TAG = "Ghost";

    private void Awake() {
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMoveKeyboard();
        AnimatePlayer();
       
    }

     private void FixedUpdate()
    {
         PlayerJump();
    }

    void PlayerMoveKeyboard(){
        movementX = Input.GetAxis("Horizontal");
        transform.position += new Vector3(movementX, 0f, 0f) *  moveForce * Time.deltaTime;
    }

    void AnimatePlayer(){
        Vector3 localScale = transform.localScale;

        if(movementX > 0){
            anim.SetBool(WALK_ANIMATION, true);
            //sr.flipX = false;
            localScale.x = 3;
        }else if (movementX < 0){
            anim.SetBool(WALK_ANIMATION, true);
            localScale.x = -3;
            //sr.flipX = true;
        }else{
            anim.SetBool(WALK_ANIMATION, false);
        }   
        transform.localScale = localScale; 
    }

    void PlayerJump(){
        if (Input.GetKey("up") && isGrounded){
            isGrounded = false;
            myBody.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

     private void OnCollisionEnter2D(Collision2D collision) //collision es la hierba y el dragon
    {

        if (collision.gameObject.CompareTag(GROUND_TAG)){
            isGrounded = true;
        } 

        if (collision.gameObject.CompareTag(ENEMY_TAG)) //si el dragon me toca muero
            Destroy(gameObject);

       
    }

     private void OnTriggerEnter2D(Collider2D collision) //el collider es el ghost
    {
        
        if (collision.CompareTag(GHOST_TAG))  //si el ghost me toca muero
            Destroy(gameObject);   
    }



}
