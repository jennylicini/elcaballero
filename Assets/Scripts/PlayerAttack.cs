using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class PlayerAttack : MonoBehaviour

{
    public Transform attackPos;
    public LayerMask whatIsEnemies;
    public Animator animAttack;
    public float attackRangeX;
    public float attackRangeY;
    private Monster monster;
    public Text score;
    public Text win;


    [HideInInspector] 
    public static int muertes = 4;

    private void Awake() {
        monster = GetComponent<Monster>();
    }

    void Update()
    {
         
        if(Input.GetKey(KeyCode.Space)){
            Attack();
        }
           
    }

    void Attack(){

        //emepezar la animacion
        animAttack.SetTrigger("Attack");

        //detectar los enemigos
        Collider2D [] enemigosParaMatar = Physics2D.OverlapBoxAll(attackPos.position, new Vector2(attackRangeX, attackRangeY), 0, whatIsEnemies);

        //hacer daño
        foreach(Collider2D enemy in enemigosParaMatar) {
            if(enemy.tag == "Enemy"){
                muertes -= 1;
                setScore();
                if (muertes == 0){
                    //win.text = "HAS GANADO!";
                    StartCoroutine(WaitForSceneLoad());
                    muertes = 4;
                    //SceneManager.LoadScene("WinScene");
                }
                enemy.GetComponent<Monster>().TakeDamage();  
            }else{
                Debug.Log("no se puede matar: "+enemy.tag);
            }
                
        }
        
    }

    private IEnumerator WaitForSceneLoad() {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("WinScene");
    }


    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(attackPos.position, new Vector3(attackRangeX, attackRangeY, 1));
    }

    public void setScore(){
        score.text = muertes.ToString();
    }
}
